import logo from './logo.svg';
import './App.css';
import Pings from "./Pings";
import {Simulator} from "./Simulator";
import {useState} from "react";


function App() {
    const [light, setLight] = useState(false);
    const [textFromDevice, setTextFromDevice] = useState("");
    const [textToDevice, setTextToDevice] = useState("");

    const valueHandlerOn = (e) => {
        sendLight("on").then((resp) => {
            setLight(true);
            setTextFromDevice(resp.result)
        });

    }

    const valueHandlerOff = (e) => {
        sendLight("off").then((resp) => {
            setLight(false);
            setTextFromDevice(resp.result);
        })

    }

    const sendLight = async (status) => {
        const headers = {
            "Content-Type": "application/json"
        };
        const response = await fetch('http://localhost:8080/ping-clients/pick-by-light-send/' + status, {
            headers,
            method: "POST"
        });
        const jsonData = await response.json();
        console.log("sendlight response" + JSON.stringify(jsonData));
        return jsonData;
    }

    const getStatus = async () => {
        const headers = {
            "Content-Type": "application/json"
        };
        const response = await fetch('http://localhost:8080//ping-clients/pick-by-light/status', {
            headers,
            method: "GET"
        });
        const jsonData = await response.json();
        return jsonData;
    }

    const textChangeHandler = (e) => {
        setTextToDevice(e.target.value);
    }

    const valueHandlerSendText = (e) => {
        let newText = convertToAscii(textToDevice);
        sendLight(newText).then((resp) => {
            setTextFromDevice(resp.result);
        })
    }

    const convertToAscii = (str) => {
        var bytes = [];
        bytes.push(0x10);
        for(let i = 0; i < str.length; i++) {
            var char = str.charCodeAt(i);
            //(char >>> 8);
            bytes.push(char & 0xFF);
        }
        bytes.push(0x20);
        return bytes;
    }

    return (
        <div className="App">
            <header className="App-header">
                <Pings/>
                <Simulator light={light} textFromDevice={textFromDevice}/>
                <input placeholder="text to device" id="idText" value={textToDevice} onChange={textChangeHandler}/>
                <button onClick={valueHandlerOn}>Turn light on</button>
                <button onClick={valueHandlerOff}>Turn light off</button>
                <button onClick={valueHandlerSendText}>Send Text</button>
            </header>
        </div>
    );
}

export default App;
