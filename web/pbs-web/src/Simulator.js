import React from 'react';
import './Simulator.css'

const Simulator = (props) => {

    // const setLightSuper = (value) => {
    //     setLight(value);
    // }

    // const turnedOn = useMemo(() => {
    //     return props.light;
    // },[]);

    //const [turnedOn, setLight] = useState(false);


    return( <div className="simulator-main">
                <div className="light" style={{backgroundColor: props.light ? 'green' : 'red'}}>
                </div>
                <div>
                    <span>Modul 1 with text: {props.textFromDevice}</span>
                </div>
            </div>)
}

export {Simulator};
