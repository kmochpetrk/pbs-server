import React, {useEffect, useState, useRef} from 'react';
import ReactDOM from 'react-dom';
import {Simulator} from "./Simulator";


const Pings = (props) => {

    const [pings, setPings] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/ping-clients').then(data => {
            return data.json();
        }).then(dataJson => {
            setPings(dataJson);
        })
    }, []);


    return (
        <div>
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>Ip</th>
                        <th>Ping</th>
                    </tr>
                    </thead>
                    <tbody>
                    {(pings).map((elem, i) => (
                        <tr key={i}>
                            <td>{elem.ip}</td>
                            <td style={{color: 'white', backgroundColor: elem.isReachable ? '#419128' : 'red'}}>{elem.isReachable ? 'true' : 'false'}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Pings;

