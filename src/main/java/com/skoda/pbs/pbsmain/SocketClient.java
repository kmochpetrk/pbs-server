package com.skoda.pbs.pbsmain;

import java.io.*;
import java.net.Socket;

public class SocketClient {

    private String clientName;
    private String ipAddress;
    private Integer port;
    private DataOutputStream out;
    private DataInputStream in;
    private Socket clientSocket;
    private Boolean connectTried;


    public SocketClient(String clientName, String ipAddress, Integer port, Boolean connectTried) throws IOException {
        this.clientName = clientName;
        this.ipAddress = ipAddress;
        this.port = port;
        this.connectTried = connectTried;
        if (this.connectTried) {
            connect();
        }
    }

    public void connect() throws IOException {
        clientSocket = new Socket(ipAddress, port);
        out = new DataOutputStream(clientSocket.getOutputStream());
        in = new DataInputStream(clientSocket.getInputStream());
    }

    public byte[] sendMessage(byte[] msg) throws IOException {
        out.write(msg);
        byte[] inBytes = new byte[1000];
        in.read(inBytes);
        System.out.println("Client: " + clientName + " received " + new String(inBytes));
        return inBytes;
    }

    public byte[] sendMessageE(byte[] msg) throws IOException, InterruptedException {
        out.write(msg);
        byte[] inBytes = new byte[1000];
        in.read(inBytes);
        System.out.println("Client11: " + clientName + " received " + new String(inBytes));
        out.write(new byte[]{
                0x02,
                0x30,0x30,0x32,
                0x30,0x30,0x30,0x36,
                0x54,0x30,0x30,0x30,0x32,0x31,
                0x03});

        byte[] inBytes2 = new byte[1000];
        in.read(inBytes2);
        System.out.println("Client12: " + clientName + " received " + new String(inBytes2));
        Thread.sleep(20000);
        out.write(new byte[]{
                0x02,
                0x30,0x30,0x33,
                0x30,0x30,0x30,0x36,
                0x54,0x30,0x30,0x30,0x32,0x30,
                0x03});

        byte[] inBytes3 = new byte[1000];
        in.read(inBytes3);
        System.out.println("Client13: " + clientName + " received " + new String(inBytes3));
        return inBytes3;
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public String getClientName() {
        return clientName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Integer getPort() {
        return port;
    }

    public DataOutputStream getOut() {
        return out;
    }

    public DataInputStream getIn() {
        return in;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }
}
