package com.skoda.pbs.pbsmain;

import lombok.Data;

@Data
public class ModuleState {
    private boolean lightOn = false;
    private String textToDisplay = "";
}
