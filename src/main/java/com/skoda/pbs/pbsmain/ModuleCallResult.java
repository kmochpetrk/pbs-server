package com.skoda.pbs.pbsmain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ModuleCallResult {
    private Integer[] result;
}
