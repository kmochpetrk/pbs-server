package com.skoda.pbs.pbsmain;

import com.fasterxml.jackson.core.util.ByteArrayBuilder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@Slf4j
public class MainListener {


    @Value("${max_listen_threads}")
    private Integer maxThreads;

    @Value("${listen_port}")
    private Integer listenPort;

    private ExecutorService executorService;

    private ModuleState moduleState = new ModuleState();

    private List<ClientHandler> clientHandlers = new ArrayList<>();
    private List<String> topics = new ArrayList<>();
    private Map<String, List<Message>> topicsMessages = new HashMap<>();

    private Map<String, SocketClient> mapNameToClientSocket = new ConcurrentHashMap();


    @PostConstruct
    public void postConstruct() throws IOException, InterruptedException {
        executorService = Executors.newFixedThreadPool(maxThreads);
        MainLoop mainLoop = new MainLoop(listenPort, executorService, clientHandlers, topicsMessages, moduleState);
        Thread mainThread = new Thread(mainLoop);
        mainThread.start();
        Thread.sleep(2000);
        startClients();
    }

    private void startClients() throws IOException {
        mapNameToClientSocket.put("A", new SocketClient("A", "127.0.0.1", listenPort, true));
        mapNameToClientSocket.put("B", new SocketClient("B", "127.0.0.1", listenPort, true));
        mapNameToClientSocket.put("C", new SocketClient("C", "192.168.0.1", listenPort, false));
        mapNameToClientSocket.put("D", new SocketClient("D", "192.168.0.2", listenPort, false));
        mapNameToClientSocket.put("E", new SocketClient("E", "192.168.136.1", 15003, true));
    }

    public ModuleState getModuleState() {
        return moduleState;
    }

    @Scheduled(initialDelay = 5000, fixedRate = 1000000)
    public void sendClientMessages() throws IOException, InterruptedException {
        mapNameToClientSocket.get("A").sendMessage("123".getBytes());
        mapNameToClientSocket.get("A").sendMessage("456".getBytes());

        mapNameToClientSocket.get("B").sendMessage("123".getBytes());

        mapNameToClientSocket.get("A").sendMessage("891".getBytes());
        // P100000112345
        mapNameToClientSocket.get("E").sendMessageE(new byte[]{
                0x02,
                0x30,0x30,0x31,
                0x30,0x30,0x31,0x33,
                0x50,0x31,0x30,0x30,0x30,0x30,0x30,0x31,0x30,0x30, 0x33,0x34,0x35,
                0x03
                });

    }


    private static class MainLoop implements Runnable {

        private Integer listenPort;
        private ExecutorService executorService;
        private List<ClientHandler> clientHandlers;
        private Map<String, List<Message>> topicsMessages;
        private ModuleState moduleState;


        public MainLoop(Integer listenPort, ExecutorService executorService, List<ClientHandler> clientHandlersParam,
                        Map<String, List<Message>> topicsMessagesPar, ModuleState moduleStatePar) {
            this.listenPort = listenPort;
            this.executorService = executorService;
            clientHandlers = clientHandlersParam;
            topicsMessages = topicsMessagesPar;
            moduleState = moduleStatePar;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    ServerSocket serverSocket = new ServerSocket(listenPort);
                    while (true) {
                        Socket socketClient = null;
                        try {
                            socketClient = serverSocket.accept();
                        } catch (IOException e) {
                            System.out.println("I/O error: " + e);
                        }
                        // new thread for a client
                        ClientHandler runnable = new ClientHandler(socketClient, topicsMessages, moduleState);
                        clientHandlers.add(runnable);
                        executorService.execute(runnable);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    private static class ClientHandler extends Thread {
        private Socket serverSocket;
        private OutputStream out;
        private Map<String, List<Message>> topicsMessages;
        private ModuleState moduleState;
        //private BufferedReader in;

        public ClientHandler(Socket serverSocket, Map<String, List<Message>> topicsMessagesPar, ModuleState moduleStatePar) {
            this.serverSocket = serverSocket;
            topicsMessages = topicsMessagesPar;
            moduleState = moduleStatePar;
        }

        @SneakyThrows
        public void run() {
            while (true) {
                out = serverSocket.getOutputStream();
//            in = new BufferedReader(
//                    new InputStreamReader(serverSocket.getInputStream()));

//            String inputLine;
//            while ((inputLine = in.readLine()) != null) {
//                if ("123".equals(inputLine)) {
//                    out.println("bye1");
//                } else
//                if ("456".equals(inputLine)) {
//                    out.println("bye2");
//                } else {
//                    out.println(inputLine);
//                }
//            }

                //byte[] bytes =  new byte[200]; //serverSocket.getInputStream().readAllBytes();

                var in = new DataInputStream(serverSocket.getInputStream());
                byte[] data = new byte[16384];
                var baos = new ByteArrayOutputStream();
                baos.write(data, 0, in.read(data));
                byte[] received = baos.toByteArray();
                String receivedStr = Arrays.toString(received);
                log.info("Server received: " + receivedStr);

                var dos = new DataOutputStream(out);
                List<Byte> dataIn = new ArrayList<>();
                if (received[0] == 0x10) {
                    int i = 0;
                    while (received[++i] != 0x20) {
                        dataIn.add(received[i]);
                    }
                }
                String dataInStr = Arrays.toString(dataIn.toArray());
                log.info("Server received data: " + dataInStr);
                byte[] serverOut = null;
                if (dataIn.size() > 1) { // text
                    moduleState.setTextToDisplay(dataInStr);
                    serverOut = new byte[dataIn.size() + 2];
                    serverOut[0] = 0x10;
                    for (int i = 0; i < dataIn.size(); i++) {
                        serverOut[i + 1] = dataIn.get(i);
                    }
                    serverOut[serverOut.length - 1] = 0x20;
                } else if (dataIn.isEmpty()) {
                    log.error("Null data received by module!");
                    serverOut = new byte[]{0x10, 0x6D, 0x20};
                } else if (dataIn.get(0) == 0x11) {  //on
                    moduleState.setLightOn(true);
                    serverOut = new byte[]{0x10, 0x6F, 0x20};
                } else if (dataIn.get(0) == 0x22) {  //off
                    moduleState.setLightOn(false);
                    serverOut = new byte[]{0x10, 0x6E, 0x20};
                } else {
                    log.error("Bad data received by module: " + dataIn.get(0));
                    serverOut = new byte[]{0x10, 0x6D, 0x20};
                }

//                    byte[] subs = Arrays.copyOfRange(result, 3, 3 + result[2]);
//                    String topic = new String(subs);
//                    log.info("Server publish topic:" + topic);
//                    byte[] subs2 = Arrays.copyOfRange(result, 3 + result[2] + 2, 3 + result[2] + 2 + result[3 + result[2] + 1]);
//                    String mes = new String(subs2);
//                    log.info("Server publish topic message:" + mes);
//                    if (topicsMessages.get(topic) == null) {
//                        topicsMessages.put(topic, List.of(new Message(mes, subs2, false)));
//                    } else {
//                        topicsMessages.get(topic).add(new Message(mes, subs2, false));
//                    }
//                }
                log.info("Server Bytes number: " + serverOut.length);

                //in.close();

                dos.write(serverOut);
                //out.close();
                //serverSocket.close();
            }
        }
    }


    public Map<String, SocketClient> getMapNameToClientSocket() {
        return mapNameToClientSocket;
    }
}
