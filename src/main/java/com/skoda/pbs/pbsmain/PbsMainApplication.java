package com.skoda.pbs.pbsmain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PbsMainApplication {




    public static void main(String[] args) {
        SpringApplication.run(PbsMainApplication.class, args);
    }

}
