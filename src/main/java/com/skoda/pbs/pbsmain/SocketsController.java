package com.skoda.pbs.pbsmain;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/ping-clients")
@CrossOrigin
public class SocketsController {

    @Autowired
    private MainListener mainListener;

    @RequestMapping
    public ResponseEntity getPingStatuses() throws IOException {
        List<PingResultElem> resultElems = new ArrayList<>();
        Map<String, SocketClient> mapNameToClientSocket = mainListener.getMapNameToClientSocket();
        for (Map.Entry<String, SocketClient> entry : mapNameToClientSocket.entrySet()) {
            String name = entry.getKey();
            SocketClient clientSocket = entry.getValue();
            InetAddress byName = InetAddress.getByName(clientSocket.getIpAddress());
            boolean reachable = byName.isReachable(1000);
            resultElems.add(new PingResultElem(clientSocket.getIpAddress(), reachable));
        }

        log.info("ping result: " + resultElems);
        return new ResponseEntity(resultElems, HttpStatus.OK);
    }

    @Value
    public static class PingResultElem {
        private String ip;
        private Boolean isReachable;
    }


    @RequestMapping(value = "/pick-by-light/status")
    public ResponseEntity<ModuleState> getPickByLight() {
        ModuleState moduleState = mainListener.getModuleState();
        return new ResponseEntity<>(moduleState, HttpStatus.OK);
    }

    @RequestMapping(value = "/pick-by-light-send/{lightStatusOrText}", method = RequestMethod.POST)
    public ResponseEntity<ModuleCallResult> setPickByLight(@PathVariable String lightStatusOrText) throws IOException {
        byte[] bytesOut = null;
        if (lightStatusOrText.equals("on")) {
            bytesOut = new byte[]{0x10, 0x11, 0x20};
        } else if (lightStatusOrText.equals("off")) {
            bytesOut = new byte[]{0x10, 0x22, 0x20};
        } else {
            List<Byte> collect = Arrays.stream(lightStatusOrText.split(",")).map(a -> new Integer(a)).map(b -> b.byteValue()).collect(Collectors.toList());
            bytesOut = new byte[collect.size()];
            for (int i = 0; i < collect.size(); i++) {
                bytesOut[i] = collect.get(i).byteValue();
            }

        }
        byte[] resultBytes = mainListener.getMapNameToClientSocket().get("A")
                .sendMessage(bytesOut);
        List<Integer> resultList = new ArrayList<>();
        for (byte oneByte : resultBytes) {
            int oneInt = new Byte(oneByte).intValue();
            if (oneInt > 0) {
                resultList.add(oneInt);
            }
        }
        return new ResponseEntity<>(new ModuleCallResult(resultList.toArray(new Integer[]{})), HttpStatus.OK);
    }
}
