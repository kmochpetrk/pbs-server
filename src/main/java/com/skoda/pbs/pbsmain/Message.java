package com.skoda.pbs.pbsmain;

import lombok.Value;

@Value
public class Message {
    private String test;
    private byte[] bytes;
    private boolean sent;
}
