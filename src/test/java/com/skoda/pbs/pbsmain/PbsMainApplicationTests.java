package com.skoda.pbs.pbsmain;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.CollectionUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@Slf4j
class PbsMainApplicationTests {

    //@Test
    void contextLoads() throws IOException, InterruptedException {
        SocketClient client1 = new SocketClient("A", "127.0.0.1", 6969, true);
        Thread.sleep(5000);
        client1.connect();
        DataInputStream dis = new DataInputStream(client1.getIn());

        DataOutputStream dataOutputStream = new DataOutputStream(client1.getOut());
        byte[] clientOut = {0x10, 0x0, 0x0};
        dataOutputStream.write(clientOut);

        byte[] data = new byte[16384];
        var baos = new ByteArrayOutputStream();
        baos.write(data, 0, dis.read(data));

        byte[] result = baos.toByteArray();
        String resultStr = Arrays.toString(result);
        log.info("Client Result: " + resultStr);

        log.info("Client Bytes number: " + clientOut.length);

        String topic = "test";
        String message = "Ahoj";

        List<Byte> bytes = new ArrayList<>();
        bytes.add((byte) 0x30);
        bytes.add((byte) 0x0);
        bytes.add((byte) topic.length());
        for (int i = 0; i < topic.length(); i++) {
            bytes.add((byte) topic.charAt(i));
        }
        bytes.add((byte) 0x0);
        bytes.add((byte) message.length());
        for (int i = 0; i < message.length(); i++) {
            bytes.add((byte) message.charAt(i));
        }
        byte[] bytes2 = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            bytes2[i] = (byte) bytes.get(i);
        }
        dataOutputStream.write(bytes2);



        //SocketClient client2 = new SocketClient("B", "127.0.0.1", 6969,true);
        //client2.connect();

        //String msg3 = client2.sendMessage("123");
        //String msg4 = client1.sendMessage("891");
        //String terminate = client1.sendMessage(".");

//        assertEquals(msg1, "hello");
//        assertEquals(msg2, "world");
//        assertEquals(terminate, "bye");
    }

}
